<?php

class WhiteRabbit
{
    public function findMedianLetterInFile($filePath)
    {
        return array("letter"=>$this->findMedianLetter($this->parseFile($filePath),$occurrences),"count"=>$occurrences);
    }

    /**
     * Parse the input file for Chars.
     * @param $filePath
     */
    private function parseFile ($filePath)
    {
        //TODO implement this!

        //The file gets convertet to a string of chars, and then to lowercase


        
        $fileToString = file_get_contents($filePath);

        $fileToString = strtolower(preg_replace('/[^A-Za-z]/', '', $fileToString));

        return $fileToString;
    }

    /**
     * Return the letter whose occurrences are the median.
     * @param $parsedFile
     * @param $occurrences
     */
    private function findMedianLetter($parsedFile, &$occurrences)
    {
        //TODO implement this!

        $textArray = count_chars($parsedFile, 1);

        asort($textArray);

        $keys = array_keys($textArray);
        $index = floor(((count($keys)-1) / 2));

        $letter = chr($keys[$index]);

        $occurrences = $textArray[$keys[$index]];

        return $letter;
    }
}