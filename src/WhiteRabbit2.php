<?php

class WhiteRabbit2
{
    /**
     * return a php array, that contains the amount of each type of coin, required to fulfill the amount.
     * The returned array should use as few coins as possible.
     * The coins available for use is: 1, 2, 5, 10, 20, 50, 100
     * You can assume that $amount will be an int
     */
    public function findCashPayment($amount){

            $coinamount = array('1' => 0, '2' => 0, '5' => 0, '10' => 0, '20' => 0, '50' => 0, '100' => 0 );

            // Amount will repeat through the while loop until it reaches 0, and then the array will use as few coins as possible 
    
            while ($amount > 0) {
    
                if ($amount >= 100) {
    
                    $coinamount['100']++;
                    $amount = $amount - 100;
    
                } else if ($amount >= 50) {
    
                    $coinamount['50']++;
                    $amount = $amount - 50;
    
                } else if ($amount >= 20) {
    
                    $coinamount['20']++;
                    $amount = $amount - 20;
    
                } else if ($amount >= 10) {
    
                    $coinamount['10']++;
                    $amount = $amount - 10;
    
                } else if ($amount >= 5) {
    
                    $coinamount['5']++;
                    $amount = $amount - 5;
    
                } else if ($amount >= 2) {
    
                    $coinamount['2']++;
                    $amount = $amount - 2;
    
                } else if ($amount >= 1) {
    
                    $coinamount['1']++;
                    $amount = $amount - 1;
    
                }
            }
            return $coinamount;
    }
}