<?php

namespace test;

require_once(__DIR__ . "/../src/WhiteRabbit3.php");

use PHPUnit_Framework_TestCase;
use WhiteRabbit3;

class WhiteRabbit3Test extends \PHPUnit\Framework\TestCase
{
    /** @var WhiteRabbit3 */
    private $whiteRabbit3;

    public function setUp():void
    {
        parent::setUp();
        $this->whiteRabbit3 = new WhiteRabbit3();

    }

    //SECTION FILE !
    /**
     * @dataProvider multiplyProvider
     */
    public function testMultiply($expected, $amount, $multiplier){
        $this->assertEquals ($expected, $this->whiteRabbit3->multiplyBy($amount, $multiplier));
    }

    public function multiplyProvider(){
        return array(
            array(4, 2, 2),
            array(6, 3, 2),
            array(14, 7, 2),
            array(1.1, 0.55, 2)
        );

            /*
            In the array(14, 7, 2) test, the function runs $guess = abs($amount-7);, which means that $guess = abs(7-7);. This is of course 0.
            In the while loop next, while(abs($estimatedResult - $amount) > 0.49 && $guess != 0) one of the conditions is that guess is not equal 0, but it is in this case, 
            and will then just return $amount which is 7 and not the expected 14. 


            In the array(1.1, 0.55, 2)) test, the function will never be correct if the result is expected to be a decimal number, because the return value is round($amount);
            Which means that $amount will get rounded up or down, so it can never be equal to estimatedResult if estimatedResult is a decimal number

            */
 
    }
}


